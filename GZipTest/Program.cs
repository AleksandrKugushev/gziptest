﻿using GZipTest.Archivation;
using System;
using System.IO;
using System.Security;

namespace GZipTest
{
    class Program
    {
        static void Main(string[] args)
        {      
            try
            {
                DoWork(args);
            }
            catch (Exception)
            {
                Console.WriteLine("This Program Has Performed an Illegal Operation and Will Be Shut Down");
                throw; // we can read this exception from windows log
            }            
        }

        private static void DoWork(string[] args)
        {
            if (args.Length != 3)
            {
                ShowInfo();
                return;
            }

            bool success;
            switch (args[0])
            {
                case "compress":
                    success = Process(args[1], args[2], new Archiver().Compress);
                    break;
                case "decompress":
                    success = Process(args[1], args[2], new Archiver().Decompress);
                    break;
                default:
                    ShowInfo();
                    return;
            }
            Console.WriteLine(success ? 1 : 0);
            Console.ReadLine();
        }

        private static void ShowInfo()
        {
            Console.WriteLine("Incorrect input!");
            Console.WriteLine("Compress - GZipTest.exe compress [file name] [archive name]");
            Console.WriteLine("Compress - GZipTest.exe decompress [archive name] [file name]");
        }

        private static bool Process(string source, string target, Action<Stream, Stream> operation)
        {
            try
            {
                using (var sourceStream = new FileStream(source, FileMode.Open))
                using (var targetStream = new FileStream(target, FileMode.Create))                
                    operation(sourceStream, targetStream);
                return true;
            }
            catch (ArchivationException e)
            {
                Console.WriteLine("Archivation error: {0}", e.Message);                
            }
            catch (NotSupportedException)
            {
                Console.WriteLine("Path not supported in current enviroment");
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Unauthorized access error. Possible file is read only");
            }
            catch (SecurityException)
            {
                Console.WriteLine("Security error");
            }
            catch (IOException)
            {
                Console.WriteLine("IO error. Possible file not found or path too long");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Incorrect file name or archive name");
            }

            return false;
        }        
    }
}
