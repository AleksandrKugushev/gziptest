﻿using System;
using System.Threading;

namespace GZipTest.Misc
{
    class LockFreeQueue<T>
    {
        private volatile Node _head;
        private volatile Node _tail;

        public LockFreeQueue()
        {
            _head = new Node();
            _tail = _head;
        }

        public event Action Enqueued = () => { };
        public event Action Dequeued = () => { };

        public void Enqueue(T value)
        {
            Node node = new Node();
            node.Value = value;

            Node tail, next;
            while (true)
            {
                tail = _tail;
                next = tail.Next;
                if (tail == _tail)
                {
                    if (next == null)
                    {
                        if (Interlocked.CompareExchange(ref tail.Next, node, next) == next)
                            break;
                    }
                    else
                        Interlocked.CompareExchange(ref _tail, next, tail);
                }
            }
            Interlocked.CompareExchange(ref _tail, node, tail);
            Enqueued();
        }

        public bool TryDequeue(out T value)
        {
            while (true)
            {
                Node head = _head;
                Node tail = _tail;
                Node next = head.Next;
                if (head == _head)
                {
                    if (head == tail)
                    {
                        if (next == null)
                        {
                            value = default(T);
                            return false;
                        }
                        Interlocked.CompareExchange(ref _tail, next, tail);
                    }
                    else
                    {
                        value = next.Value;
                        if (Interlocked.CompareExchange(ref _head, next, head) == head)
                            break;
                    }
                }
            }
            Dequeued();
            return true;
        }

        class Node
        {
            public T Value;
            public Node Next;
        }
    }
}
