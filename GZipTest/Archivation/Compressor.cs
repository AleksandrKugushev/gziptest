﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;

namespace GZipTest.Archivation
{
    class Compressor : StreamProcessor
    {
        public Compressor(Stream source, Stream target)
            : base(source, target)
        { }

        internal override void ReadSource()
        {
            int index = 0;

            byte[] bytes = new byte[SliceSize];
            int length;
            while ((length = Source.Read(bytes, 0, bytes.Length)) != 0)
            {
                if (length != SliceSize)
                    bytes = bytes.Take(length).ToArray();
                                            
                SourceQueue.Enqueue(new Block(index++, bytes));
                Debug.WriteLine("Read " + index);             

                CalcQueuesMaxSize();
                while (QueuesCurrentSize > QueuesMaxSize) // bad, but simple solution
                    Thread.Sleep(300);
            }
        }

        internal override byte[] ProcessBytes(byte[] bytes)
        {
            return Compress(bytes);            
        }

        internal override void WriteToStream(byte[] bytes)
        {
            byte[] size = BitConverter.GetBytes(bytes.Length);
            Target.Write(size, 0, size.Length);
            Target.Write(bytes, 0, bytes.Length);
        }

        private byte[] Compress(byte[] bytes)
        {
            using (var stream = new MemoryStream())
            {
                using (var gzip = new GZipStream(stream, CompressionMode.Compress, true))
                    gzip.Write(bytes, 0, bytes.Length);

                return stream.ToArray();
            }
        }
    }
}
