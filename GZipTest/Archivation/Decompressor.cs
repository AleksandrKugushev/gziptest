﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;

namespace GZipTest.Archivation
{
    class Decompressor : StreamProcessor
    {
        public Decompressor(Stream source, Stream target)
            : base(source, target)
        { }

        internal override void ReadSource()
       {
            int index = 0;
            int read;
            do
            {
                byte[] size = new byte[4];
                Source.Read(size, 0, size.Length);
                int length = BitConverter.ToInt32(size, 0);

                byte[] bytes = new byte[length];
                read = Source.Read(bytes, 0, length);
                if (read != length)
                    throw new Exception();
                                
                SourceQueue.Enqueue(new Block(index++, bytes));
                Debug.WriteLine("Read " + index);

                CalcQueuesMaxSize();
                while (QueuesCurrentSize > QueuesMaxSize) // bad, but simple solution
                    Thread.Sleep(300);
            } while (read > 0);
        }

        internal override byte[] ProcessBytes(byte[] bytes)
        {
            return Decompress(bytes);
        }

        internal override void WriteToStream(byte[] bytes)
        {
            Target.Write(bytes, 0, bytes.Length);
        }

        private byte[] Decompress(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            using (var gzip = new GZipStream(stream, CompressionMode.Decompress))
            {
                byte[] buffer = new byte[SliceSize];
                int length = gzip.Read(buffer, 0, buffer.Length);
                if (length < buffer.Length)
                    buffer = buffer.Take(length).ToArray();

                return buffer;
            }
        }

    }
}

