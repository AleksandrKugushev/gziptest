﻿using System.Collections.Generic;
using System.Threading;

namespace GZipTest.Misc
{
    class ThreadLocal<T>
    {
        private Dictionary<int, T> _values = new Dictionary<int, T>();
        private ReaderWriterLockSlim _sync = new ReaderWriterLockSlim();

        public T Value
        {
            get
            {
                _sync.EnterReadLock();
                try
                {
                    T result;
                    if (_values.TryGetValue(Thread.CurrentThread.ManagedThreadId, out result))
                        return result;
                    return default(T);
                }
                finally
                {
                    _sync.ExitReadLock();
                }                
            }
            set
            {
                _sync.EnterWriteLock();
                try
                {
                    _values[Thread.CurrentThread.ManagedThreadId] = value;
                }
                finally
                {
                    _sync.ExitWriteLock();
                }                
            }
        }
    }
}
