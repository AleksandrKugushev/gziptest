﻿using System;

namespace GZipTest.Archivation
{
    class ArchivationException : Exception
    {
        public ArchivationException(string friendlyMessage, Exception innerException)            
            : base(friendlyMessage, innerException)
        {            
        }                
    }
}
