﻿using System;
using System.IO;

namespace GZipTest.Archivation
{
    public class Archiver
    {
        public void Compress(Stream source, Stream target)
        {
            Execute(() =>
            {
                using (var compressor = new Compressor(source, target))
                    compressor.Start();
            });
        }

        public void Decompress(Stream source, Stream target)
        {
            Execute(() =>
            {
                using (var decompressor = new Decompressor(source, target))
                    decompressor.Start();
            });
        }

        private void Execute(Action action)
        {
            try
            {
                action();
            }
            catch (ArgumentException e)
            {
                throw new ArchivationException("Invalid input", e);
            }
            catch (Exception e)
            {
                throw new ArchivationException("Operation was canceled. " + e.Message, e);
            }
        }
    }
}
