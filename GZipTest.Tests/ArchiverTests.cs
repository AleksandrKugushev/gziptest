﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;
using GZipTest.Archivation;

namespace GZipTest.Tests
{
    /// <summary>
    /// Simple integration test
    /// </summary>
    [TestClass]
    public class ArchiverTests
    {
        [TestMethod]
        public void Compres_TestBytes()
        {
            // arrange
            const int max = 100000000;
            byte[] bytes = Enumerable.Range(0, max).Select(i => (byte)i).ToArray();           

            // act            
            byte[] compressed = null;            
            using (var source = new MemoryStream(bytes))
            using (var target = new MemoryStream())
            {
                var archiver = new Archiver();
                archiver.Compress(source, target);
                compressed = target.ToArray();
            }

            byte[] decompressed = null;
            using (var source = new MemoryStream(compressed))
            using (var target = new MemoryStream())
            {
                var archiver = new Archiver();
                archiver.Decompress(source, target);
                decompressed = target.ToArray();
            }

            // assert            
            CollectionAssert.AreEqual(bytes, decompressed);
        }      
    }
}
