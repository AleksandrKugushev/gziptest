﻿using System.Threading;

namespace GZipTest.Misc
{
    class CountdownEvent
    {
        private volatile int _currentCount;
        private ManualResetEvent _waitHandle;

        public CountdownEvent(int initalCount)
        {
            _currentCount = initalCount;
            _waitHandle = new ManualResetEvent(initalCount == 0);
        }   
        
        public bool Signal()
        {
            int current, changed;
            do
            {
                current = _currentCount;
                changed = current - 1;

                if (changed < 0)
                    return true;                
            } while ((Interlocked.CompareExchange(ref _currentCount, changed, current) != current));

            if (changed == 0)            
                return _waitHandle.Set();
            return false;            
        }

        public void Reset()
        {
            _currentCount = 0;
            _waitHandle.Set();
        }

        public void Wait()
        {
            _waitHandle.WaitOne();
        }
    }
}
