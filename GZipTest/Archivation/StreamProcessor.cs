﻿using GZipTest.Misc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace GZipTest.Archivation
{
    abstract class StreamProcessor : IDisposable
    {
        protected const int InitialQueueMaxSize = 32;
        protected const int SliceSize = 4194304;

        protected readonly Stream Source;
        protected readonly Stream Target;
        protected readonly Thread[] Threads;
        protected readonly LockFreeQueue<Block> SourceQueue = new LockFreeQueue<Block>();
        protected readonly ThreadLocal<Queue<Block>> ThreadsQueues = new ThreadLocal<Queue<Block>>();
        protected volatile int QueuesMaxSize = InitialQueueMaxSize;
        protected volatile int QueuesCurrentSize;
        protected bool SourceFinished;
        protected CountdownEvent FinishLocker;

        private volatile int _targetIndex = 0;
        private Exception _workerThreadException;
        private object _workerThreadExceptionSync = new object();
        private bool _stopped;

        protected StreamProcessor(Stream source, Stream target)
        {
            if (source == null) throw new ArgumentNullException("source"); // nameof :(
            if (target == null) throw new ArgumentNullException("target");
            if (!source.CanRead) throw new ArgumentException("Argument 'source' is not readable");
            if (!target.CanWrite) throw new ArgumentException("Argument 'target' is not writable");

            Source = source;
            Target = target;
            Threads = new Thread[Environment.ProcessorCount - 1];

            CalcQueuesMaxSize();
            SourceQueue.Enqueued += () => Interlocked.Increment(ref QueuesCurrentSize);
            SourceQueue.Dequeued += () => Interlocked.Decrement(ref QueuesCurrentSize);
        }

        public void Start()
        {
            SourceFinished = false;
            FinishLocker = new CountdownEvent(Threads.Length);
            StartThreads();

            ReadSource();
            SourceFinished = true;

            // we're finished reading, so main thread can process data
            DoProcess();

            FinishLocker.Wait();
            if (_workerThreadException != null)
                throw new Exception("Exception caught in the working thread: " + _workerThreadException.Message, _workerThreadException);
        }

        public void Dispose()
        {
            _stopped = true;
        }

        internal abstract void ReadSource();

        internal abstract byte[] ProcessBytes(byte[] bytes);

        internal abstract void WriteToStream(byte[] bytes);

        protected void CalcQueuesMaxSize()
        {
            long maxMemory = Process.GetCurrentProcess().PeakWorkingSet64;
            QueuesMaxSize = Math.Max((int)(maxMemory / (Environment.ProcessorCount * SliceSize)), InitialQueueMaxSize);
        }

        private void StartThreads()
        {
            if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                Thread.CurrentThread.Name = "main";
            for (int i = 0; i < Threads.Length; i++)
            {
                var thread = new Thread(ThreadWork);
                thread.IsBackground = true;
                thread.Name = string.Format("worker{0}", i);
                thread.Start();
                Threads[i] = thread;
            }
        }

        private void ThreadWork()
        {
            try
            {
                DoProcess();
                FinishLocker.Signal();
            }
            catch (Exception e)
            {
                lock (_workerThreadExceptionSync)
                {
                    AbortThreads();
                    _stopped = true;
                    _workerThreadException = e;
                    FinishLocker.Reset();
                }
            }
        }

        private void DoProcess()
        {
            ThreadsQueues.Value = new Queue<Block>();
            while (!_stopped)
            {
                Block block;
                if (SourceQueue.TryDequeue(out block))
                {
                    byte[] processed = ProcessBytes(block.Value);
                    ThreadsQueues.Value.Enqueue(new Block(block.Index, processed));
                    Interlocked.Increment(ref QueuesCurrentSize);
                    Debug.WriteLine("Process " + block.Index + ";\t" + Thread.CurrentThread.Name);
                }
                else
                {                    
                    if (SourceFinished && ThreadsQueues.Value.Count == 0)
                        break;
                }

                WriteData(ThreadsQueues.Value);
            }
        }

        private void WriteData(Queue<Block> value)
        {
            while (value.Count > 0)
            {
                Block block = value.Peek();
                if (block.Index == _targetIndex) // because indexes are unique, we don't need CAS, locks, etc.               
                {
                    WriteToStream(block.Value);

                    value.Dequeue();
                    Interlocked.Decrement(ref QueuesCurrentSize);

                    Interlocked.Increment(ref _targetIndex);
                    Debug.WriteLine("Write " + block.Index + ";\t" + Thread.CurrentThread.Name);
                }
                else
                    break;
            }
        }

        private void AbortThreads()
        {
            foreach (var thread in Threads)
                if (thread != Thread.CurrentThread)
                    thread.Abort();
        }

        [DebuggerDisplay("{Index}")]
        protected class Block
        {
            public Block(int index, byte[] value)
            {
                Index = index;
                Value = value;
            }

            public readonly int Index;
            public readonly byte[] Value;
        }
    }
}
